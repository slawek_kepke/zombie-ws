const zombieController = require('controllers/Zombie');
const zombieModel = require('models/Zombie');
const sinon = require('sinon');

describe('Zombie controller', () => {
    it('should add new zombie', (done) => {
        const zombieMock = {
            name: 'zombie_name',
        };
        const zombieModelMock = sinon.mock(zombieModel);
        zombieModelMock
            .expects('create')
            .once()
            .returns(zombieMock);

        const res = {
            status: () => ({
                send: (data) => {
                    data.should.be.deepEqual(zombieMock);
                    zombieModelMock.verify();
                    zombieModelMock.restore();
                    done();
                },
            }),
        };
        const req = {
            swagger: {
                params: {
                    body: {
                        value: {},
                    },
                },
            },
        };
        zombieController.addZombie(req, res);
    });
});
