const nock = require('nock');
const config = require('config');
const nbpApiResponse = require('./data/nbpApiResponse');

nock(config.nbpApi.baseUrl)
    .get('/exchangerates/tables/C/')
    .once()
    .reply(200, nbpApiResponse);
