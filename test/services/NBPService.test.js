const nbpService = require('services/NBPService');
const nbpApiResponse = require('../data/nbpApiResponse');

describe('Zombie controller', () => {
    it('should return currency rate', () => {
        const usdData = nbpApiResponse[0].rates.find(data => data.code === 'USD');
        nbpService.getRate('USD').should.be.equal(usdData.ask);
    });
});
