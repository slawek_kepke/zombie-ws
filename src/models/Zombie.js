const mongoose = require('mongoose');

const zombieSchema = new mongoose.Schema({
    name: String,
    createTime: Number,
});

zombieSchema.set('toObject', { versionKey: false, getters: true });
zombieSchema.set('toJSON', { versionKey: false, getters: true });

const Zombie = mongoose.model('Zombie', zombieSchema);

module.exports = Zombie;
