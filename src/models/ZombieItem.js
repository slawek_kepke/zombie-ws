const mongoose = require('mongoose');

const zombieItemSchema = new mongoose.Schema({
    zombieId: String,
    itemId: Number,
});

zombieItemSchema.set('toObject', { versionKey: false, getters: true });
zombieItemSchema.set('toJSON', { versionKey: false, getters: true });

const ZombieItem = mongoose.model('ZombieItem', zombieItemSchema);

module.exports = ZombieItem;
