const zombieModel = require('models/Zombie');
const zombieItemService = require('services/ZombieItemService');

class ZombieService {
    addZombie(resource) {
        return zombieModel.create({
            ...resource,
            ...{
                createTime: Date.now(),
            },
        });
    }

    async updateZombie(id, resource) {
        await zombieModel.update({ _id: id }, resource);
        return zombieModel.findById({ _id: id });
    }

    async getZombie(id) {
        let zombie = await zombieModel.findById(id);
        if (zombie) {
            zombie = zombie.toObject();
            zombie.items = await zombieItemService.getZombieItems({ zombieId: zombie._id });
            zombie.totalValue = {
                PLN: 0,
                USD: 0,
                EUR: 0,
            };
            if (Array.isArray(zombie.items)) {
                zombie.items.forEach((item) => {
                    if (!item.price) {
                        return;
                    }
                    zombie.totalValue.PLN += item.price.PLN ? item.price.PLN : 0;
                    zombie.totalValue.USD += item.price.USD ? item.price.USD : 0;
                    zombie.totalValue.EUR += item.price.EUR ? item.price.EUR : 0;
                });
            }
        }
        return zombie;
    }

    deleteZombie(id) {
        return zombieModel.deleteOne({ _id: id });
    }
}

module.exports = new ZombieService();
