const request = require('request-promise-native');
const config = require('config');

class NBPService {
    constructor() {
        this.nbpRequest = request.defaults({
            baseUrl: config.nbpApi.baseUrl,
            json: true,
        });
        this.ratesCache = [];
        this.getRates();
    }


    async getRates() {
        try {
            const response = await this.nbpRequest('/exchangerates/tables/C/');
            this.ratesCache = response[0].rates;
        } catch (e) {
            console.log(e);
            setTimeout(() => this.getRates(), 10000);
        }
        setTimeout(() => this.getRates(), 3600000);
    }

    getRate(currency) {
        const rate = this.ratesCache.find(r => r.code === currency);
        return rate ? rate.ask : null;
    }
}

module.exports = new NBPService();
