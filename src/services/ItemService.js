const request = require('request-promise-native');
const config = require('config');

class ItemService {
    constructor() {
        this.itemRequest = request.defaults({
            json: true,
            baseUrl: config.itemsApi.baseUrl,
        });
        this.itemsCache = [];
        this.getItems();
    }

    async getItems() {
        try {
            const response = await this.itemRequest('/items');
            this.itemsCache = response.items;
            setTimeout(() => this.getItems(), (this.getSecondsToNextDay() + 60) * 1000);
        } catch (e) {
            console.log(e);
            this.getItems();
        }
    }

    getItem(id) {
        return this.itemsCache.find(item => item.id === id);
    }

    getSecondsToNextDay() {
        const d = new Date();
        const h = d.getHours();
        const m = d.getMinutes();
        const s = d.getSeconds();
        return (24 * 60 * 60) - (h * 60 * 60) - (m * 60) - s;
    }
}

const itemService = new ItemService();

module.exports = {
    getItem: id => itemService.getItem(id),
};
