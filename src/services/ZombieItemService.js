const config = require('config');
const zombieItemModel = require('models/ZombieItem');
const itemService = require('services/ItemService');
const nbpService = require('services/NBPService');

class ZombieItemService {
    async updateZombieItem(id, resource) {
        await zombieItemModel.update({ _id: id }, resource);
        return zombieItemModel.findById({ _id: id });
    }

    async getZombieItems(query) {
        let zombieItems = await zombieItemModel.find(query);
        if (zombieItems) {
            zombieItems = zombieItems.map(item => item.toObject());
            // eslint-disable-next-line
            for (const item of zombieItems) {
                const itemDef = await itemService.getItem(item.itemId);
                const { price, name } = itemDef;
                const rateEUR = await nbpService.getRate('EUR');
                const rateUSD = await nbpService.getRate('USD');
                Object.assign(item, {
                    ...name ? { name } : null,
                    ...{
                        price: {
                            ...price ? { PLN: price } : null,
                            ...price && rateEUR ? { EUR: price * rateEUR } : null,
                            ...price && rateUSD ? { USD: price * rateUSD } : null,
                        },
                    },
                });
            }
        }
        return zombieItems;
    }

    async getZombieItem(_id) {
        const zombieItems = await this.getZombieItems({ _id });
        return zombieItems && zombieItems[0] ? zombieItems[0] : null;
    }

    async addZombieItem(resource) {
        const zombieItems = await this.getZombieItems({ zombieId: resource.zombieId });
        const { maxNumber } = config.zombieItem;
        if (zombieItems.length >= maxNumber) {
            throw new Error(`Zombie can have maximum ${maxNumber} items`);
        }
        return zombieItemModel.create(resource);
    }

    deleteZombieItem(id) {
        return zombieItemModel.deleteOne({ _id: id });
    }
}

module.exports = new ZombieItemService();
