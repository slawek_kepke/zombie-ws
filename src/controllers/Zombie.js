const zombieService = require('services/ZombieService');

const addZombie = async (req, res) => {
    const body = req.swagger.params.body.value;
    try {
        const response = await zombieService.addZombie(body);
        res.status(201).send(response);
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

const deleteZombie = async (req, res) => {
    const id = req.swagger.params.id.value;
    try {
        const response = await zombieService.deleteZombie(id);
        if (response.n === 0) {
            res.status(404).send();
        }
        res.status(200).send();
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

const getZombie = async (req, res) => {
    const id = req.swagger.params.id.value;
    try {
        const response = await zombieService.getZombie(id);
        if (!response) {
            return res.status(404).send();
        }
        res.status(200).send(response);
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

const updateZombie = async (req, res) => {
    const id = req.swagger.params.id.value;
    const body = req.swagger.params.body.value;
    try {
        const response = await zombieService.updateZombie(id, body);
        res.status(200).send(response);
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

module.exports = {
    addZombie,
    updateZombie,
    getZombie,
    deleteZombie,
};
