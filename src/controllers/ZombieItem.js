const zombieItemService = require('services/ZombieItemService');

const addZombieItem = async (req, res) => {
    const body = req.swagger.params.body.value;
    try {
        const response = await zombieItemService.addZombieItem(body);
        res.status(201).send(response);
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

const deleteZombieItem = async (req, res) => {
    const id = req.swagger.params.id.value;
    try {
        const response = await zombieItemService.deleteZombieItem(id);
        if (response.n === 0) {
            res.status(404).send();
        }
        res.status(200).send();
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

const getZombieItem = async (req, res) => {
    const id = req.swagger.params.id.value;
    try {
        const response = await zombieItemService.getZombieItem(id);
        if (!response) {
            return res.status(404).send();
        }
        res.status(200).send(response);
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

const updateZombieItem = async (req, res) => {
    const id = req.swagger.params.id.value;
    const body = req.swagger.params.body.value;
    try {
        const response = await zombieItemService.updateZombieItem(id, body);
        res.status(200).send(response);
    } catch (e) {
        res.status(500).send({ message: e.toString() });
    }
};

module.exports = {
    addZombieItem,
    updateZombieItem,
    getZombieItem,
    deleteZombieItem,
};
