---
swagger: "2.0"
info:
  description: "Zombie WS"
  version: "1.0.0"
  title: "Zombie WS"
  contact:
    email: "slawek.kepke@gmail.com"
basePath: "/v1"
tags:
- name: "zombie"
  description: "zombie management"
- name: "zombie item"
  description: "zombie items management"
schemes:
- "http"
paths:
  /zombie:
    post:
      tags:
      - "zombie"
      summary: "Adds new zombie"
      description: ""
      operationId: "addZombie"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Zombie resource"
        required: true
        schema:
          $ref: "#/definitions/Zombie"
      responses:
        201:
          description: "Zombie created"
          schema:
            $ref: "#/definitions/Zombie"
        400:
          description: "Validation exception"
        500:
          description: "Server error"
      x-swagger-router-controller: "Zombie"
  /zombie/{id}:
    get:
      tags:
      - "zombie"
      summary: "Retrieves a zombie"
      description: ""
      operationId: "getZombie"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "Zombie id"
        required: true
        type: "string"
      responses:
        200:
          description: "success"
          schema:
            $ref: "#/definitions/Zombie"
        400:
          description: "Validation exception"
        404:
          description: "Zombie not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "Zombie"
    put:
      tags:
      - "zombie"
      summary: "Updates a zombie"
      description: ""
      operationId: "updateZombie"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "Zombie id"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Zombie resource"
        required: true
        schema:
          $ref: "#/definitions/Zombie"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/Zombie"
        400:
          description: "Validation exception"
        404:
          description: "Zombie not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "Zombie"
    delete:
      tags:
      - "zombie"
      summary: "Deletes a zombie"
      description: ""
      operationId: "deleteZombie"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "Zombie id"
        required: true
        type: "string"
      responses:
        200:
          description: "Success"
        400:
          description: "Validation exception"
        404:
          description: "Zombie not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "Zombie"
  /zombieItem:
    post:
      tags:
      - "zombie item"
      summary: "Adds zombie item"
      description: ""
      operationId: "addZombieItem"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Zombie item resource"
        required: true
        schema:
          $ref: "#/definitions/ZombieItem"
      responses:
        201:
          description: "Zombie item created"
          schema:
            $ref: "#/definitions/ZombieItem"
        400:
          description: "Validation exception"
        404:
          description: "Zombie or item not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "ZombieItem"
  /zombieItem/{id}:
    get:
      tags:
      - "zombie item"
      summary: "Retrieves zombie item"
      description: ""
      operationId: "getZombieItem"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "Zombie item id"
        required: true
        type: "string"
      responses:
        200:
          description: "success"
          schema:
            $ref: "#/definitions/ZombieItem"
        400:
          description: "Validation exception"
        404:
          description: "Zombie item not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "ZombieItem"
    put:
      tags:
      - "zombie item"
      summary: "Updates zombie item"
      description: ""
      operationId: "updateZombieItem"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "Zombie item id"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Zombie item resource"
        required: true
        schema:
          $ref: "#/definitions/ZombieItem"
      responses:
        200:
          description: "Success"
          schema:
            $ref: "#/definitions/ZombieItem"
        400:
          description: "Validation exception"
        404:
          description: "Zombie not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "ZombieItem"
    delete:
      tags:
      - "zombie item"
      summary: "Deletes zombie item"
      description: ""
      operationId: "deleteZombieItem"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "id"
        in: "path"
        description: "Zombie item id"
        required: true
        type: "string"
      responses:
        200:
          description: "Success"
        400:
          description: "Validation exception"
        404:
          description: "Zombie item not found"
        500:
          description: "Server error"
      x-swagger-router-controller: "ZombieItem"
definitions:
  Zombie:
    type: "object"
    required:
    - "name"
    properties:
      id:
        type: "string"
        readOnly: true
      name:
        type: "string"
      createTime:
        type: "integer"
        format: "int64"
        readOnly: true
      itemsTotalPrice:
        type: "integer"
        format: "int64"
        readOnly: true
      items:
        type: "array"
        items:
          $ref: "#/definitions/ZombieItem"
    example:
      name: "Czesio"
  ZombieItem:
    type: "object"
    required:
    - "itemId"
    - "zombieId"
    properties:
      id:
        type: "string"
        readOnly: true
      zombieId:
        type: "string"
      itemId:
        type: "integer"
        format: "int64"
      name:
        type: "string"
        readOnly: true
      price:
        type: "integer"
        format: "int64"
        readOnly: true
    example:
      itemId: 5
      zombieId: '5be3f4c22517790085a9fc3d'
