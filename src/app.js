const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const http = require('http');
const app = require('express')();
const swaggerTools = require('swagger-tools');
const jsYaml = require('js-yaml');
const config = require('config');

mongoose.connect(`mongodb://${config.mongo.host}/${config.mongo.dbName}`);
const serverPort = 8080;

const options = {
    swaggerUi: path.join(__dirname, '/swagger.json'),
    controllers: path.join(__dirname, './controllers'),
};

const spec = fs.readFileSync(path.join(__dirname, 'api/swagger.yaml'), 'utf8');
const swaggerDoc = jsYaml.safeLoad(spec);

swaggerTools.initializeMiddleware(swaggerDoc, (middleware) => {
    app.use(middleware.swaggerMetadata());
    app.use(middleware.swaggerValidator());
    app.use(middleware.swaggerRouter(options));
    app.use(middleware.swaggerUi());
    http.createServer(app).listen(serverPort, () => {
        console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
        console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
    });
});

app.use('/$', (req, res) => {
    res.redirect('/docs/');
});
