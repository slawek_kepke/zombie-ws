## Overview
Zombie WS

### Running the server
To run the server, run:

```
docker-compose up
```

To view the Swagger UI interface:

```
open http://localhost/docs
```
